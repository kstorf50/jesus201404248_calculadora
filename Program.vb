Imports System

Module Program
    Sub Main()
        Dim x, y, z As Double
        Dim opcion As Char = ""
        While Opcion <> "z"
            Console.Clear()
            Console.WriteLine("Opcion Suma.............(s)")
            Console.WriteLine("Opcion Resta.............(r)")
            Console.WriteLine("Opcion Multiplicacion.............(m)")
            Console.WriteLine("Opcion Division.............(d)")
            Console.WriteLine("Opcion Salir.............(z)")
            Try
                'Ingrese la opcion
                Opcion = Console.ReadKey.KeyChar
                Console.Clear()
                Select Case opcion
                    Case "s"
                        Console.WriteLine("Opcion seleccionada fue suma")
                        Console.Write("Ingrese primer numero")
                        x = Console.ReadLine()
                        Console.Write("Ingrese segundo numero")
                        y = Console.ReadLine()
                        z = x + y
                        Console.WriteLine("Resultado de la suma es:" & z)
                    Case "r"
                        Console.WriteLine("Opcion seleccionada fue resta")
                        Console.Write("Ingrese primer numero")
                        x = Console.ReadLine()
                        Console.Write("Ingrese segundo numero")
                        y = Console.ReadLine()
                        z = x - y
                        Console.WriteLine("Resultado de la resta es:" & z)
                    Case "m"
                        Console.WriteLine("Opcion seleccionada fue Multiplicacion")
                        Console.Write("Ingrese primer numero")
                        x = Console.ReadLine()
                        Console.Write("Ingrese segundo numero")
                        y = Console.ReadLine()
                        z = x * y
                        Console.WriteLine("Resultado de la Multiplicacion es:" & z)
                    Case "d"
                        Console.WriteLine("Opcion seleccionada fue Division")
                        Console.Write("Ingrese primer numero")
                        x = Console.ReadLine()
                        Console.Write("Ingrese segundo numero")
                        y = Console.ReadLine()
                        If y <> 0 Then
                            z = x / y
                            Console.WriteLine("Resultado de la Division es:" & z)
                        ElseIf y = 0 Then
                            Console.WriteLine("Resultado de la division entre 0 no es posible")
                        End If
                    Case "z"
                        Console.Clear()
                        Console.WriteLine("Gracias por utilizar nuestra plataforma")
                    Case Else
                        Console.Clear()
                        Console.WriteLine("Opcion ingresada no es valida, vuelva a intentar")
                End Select
            Catch ex As Exception
                Console.WriteLine(ex.Message)
            End Try
            If opcion <> "z" Then Console.WriteLine("Pulse cualquier tecla para regresar al menu")
            Console.ReadKey()
        End While

    End Sub
End Module
